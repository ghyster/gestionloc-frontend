﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { SocialUser, SocialAuthService } from '@abacritt/angularx-social-login';
import { User } from '../_models';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    private socialUserSubject: BehaviorSubject<SocialUser>;
    public currentUser: Observable<User>;
    public socialUser:  Observable<SocialUser>;

    constructor(private http: HttpClient, private authService: SocialAuthService) {
       
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser') || '{}'));
        this.currentUser = this.currentUserSubject.asObservable();
        

        this.socialUserSubject = new BehaviorSubject<SocialUser>(JSON.parse(localStorage.getItem('socialUser') || '{}'));
        this.socialUser = this.socialUserSubject.asObservable();

    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(social: SocialUser, idtoken: string) {
        return this.http.post<any>(`${environment.apiUrl}users/authenticate`, { idtoken })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('socialUser', JSON.stringify(social));
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    this.socialUserSubject.next(social);
                }

                return user;
            }));
    }

    renew(){
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next;
    }

    logout() {
        //console.log("logout called")
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('socialUser');

        this.currentUserSubject.next;
        this.authService.signOut();
    }
}
