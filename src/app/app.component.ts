import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';
//import { User } from './_models';
import { SocialUser } from '@abacritt/angularx-social-login';
import { AuthenticationService } from './_services';
import { IconService } from './_services/icon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnDestroy {
  mobileQuery: MediaQueryList;
  socialUser: SocialUser = new SocialUser;
  socialUserSubscription: Subscription;
  title="Gestion location"

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private authenticationService: AuthenticationService,
  /*private userService: UserService, */private iconService: IconService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.socialUserSubscription = this.authenticationService.socialUser.subscribe((user: SocialUser) => {
        this.socialUser = user;
    });

    this.iconService.init();
  }

  signOut(): void {
    this.authenticationService.logout();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
