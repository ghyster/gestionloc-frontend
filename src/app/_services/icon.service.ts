import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material/icon";
import { Injectable } from "@angular/core";

@Injectable()
export class IconService {
  public list!: string[];
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {}
  init() {
    this.list =["aeroplane","bread","desk-lamp","gift","insurance1","money-bag1","parking",
    "pork","shoes","taxi","university","armchair","car-repair","drawer",
    "group","insurance","money-bag","pen","power-plant","shopping-cart","tie","wallet","auction","car","electric-car",
    "hairdressing","kite","money","piggy-bank","reading","signal","toll-road","wash","balance","clapperboard",
    "euro","hammer","laundry","mortgage-loan","pill","receipt","solar-panel","train","watermelon","bank",
    "cloud-network","faucet","hand","lawn-mower","notes","pipe","reception","subway","transmission-tower",
    "wine","beach-house","coconut-drink","feeder","home","light-bulb","nuclear-plant","plane","ribbon","swimmer",
    "ui","wrench","bellboy","credit-card","first-aid-kit","hotel","lipstick","pants","plants","rocket","taxes",
    "umbrella","box","cutlery","house","meat","police-car","gas-station","parking1","underground"].sort();
    for (let entry of this.list) {
        this.matIconRegistry.addSvgIcon(entry,this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/categories/"+entry+".svg"));
    }

  }
}
