﻿export class User {
    id!: number;
    mail!: string;
    firstName!: string;
    lastName!: string;
    token!: string;
}
